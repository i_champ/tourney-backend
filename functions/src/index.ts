import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

admin.initializeApp();

export const helloWorld = functions.https.onRequest((req, res) => {
    res.send("Hello from Firebase!!");
});


export const giveGold = functions.firestore.document('User/{id}').onCreate((snap, context) => {
    return snap.ref.update({
        gold:200
    });
})

export const createTourney = functions.firestore.document('cron-job/abcd').onWrite((snap, context) => {
    
    const promArr = [];

    const p1 = admin.firestore().collection('Tourney').add({
        entryFee: "30",
        name: "3th class tourney",
        playerList: [],
        prize: [
            150,
            100,
            50
        ],
        regTime: admin.firestore.Timestamp.fromDate(new Date('01-08-2019 15:00:0000')),
        regPlayers: 0,
        startTime: admin.firestore.Timestamp.fromDate(new Date('01-08-2019 15:30:0000')),
        totPlayer: 100,
        totPrize: 300
    });
    promArr.push(p1);
    
    const p2 = admin.firestore().collection('Tourney').add({
        entryFee: "30",
        name: "4th class tourney",
        playerList: [],
        prize: [
            150,
            100,
            50
        ],
        regTime: new Date('01-08-2019 15:00:0000'),
        regPlayers: 0,
        startTime: new Date('01-08-2019 15:30:0000'),
        totPlayer: 100,
        totPrize: 300
    });
    promArr.push(p2);

    const p3 = admin.firestore().collection('Tourney').add({
        entryFee: "40",
        name: "5th class tourney",
        playerList: [],
        prize: [
            150,
            100,
            100
        ],
        regTime: admin.firestore.Timestamp.fromDate(new Date('01-08-2019 15:00:0000')),
        regPlayers: 0,
        startTime: admin.firestore.Timestamp.fromDate(new Date('01-08-2019 15:30:0000')),
        totPlayer: 100,
        totPrize: 350
    });
    promArr.push(p3);
    
    const p4 = admin.firestore().collection('Tourney').add({
        entryFee: "50",
        name: "6th class tourney",
        playerList: [],
        prize: [
            200,
            150,
            50
        ],
        regTime: admin.firestore.Timestamp.fromDate(new Date('01-08-2019 15:00:0000')),
        regPlayers: 0,
        startTime: admin.firestore.Timestamp.fromDate(new Date('01-08-2019 15:30:0000')),
        totPlayer: 100,
        totPrize: 400
    });
    promArr.push(p4);

    const p5 = admin.firestore().collection('Tourney').add({
        entryFee: "60",
        name: "7th class tourney",
        playerList: [],
        prize: [
            200,
            150,
            100
        ],
        regTime: admin.firestore.Timestamp.fromDate(new Date('01-08-2019 15:00:0000')),
        regPlayers: 0,
        startTime: admin.firestore.Timestamp.fromDate(new Date('01-08-2019 15:30:0000')),
        totPlayer: 100,
        totPrize: 450
    });
    promArr.push(p5);



    return Promise.all(promArr);
})